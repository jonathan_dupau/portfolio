const burger = document.getElementById('menu-burger');
const nav = document.getElementById('main-menu');
const quote = document.getElementById('quote');
const back = document.getElementById('back-top');

burger.addEventListener('click', () => {
    nav.classList.toggle('show');
});

const getQuote = () => {
  let random = Math.floor(Math.random() * (1000 - 900)) + 900; 
  randomNumber = "5cd96e05de30eff6ebcce" + random.toString();
  let url = "https://the-one-api.dev/v2/quote/";
  let urlRandom = url + randomNumber;
  fetch(urlRandom, {method:'GET',
  headers: {'Authorization': 'Bearer '+ 'rQbJd3RkXyanIew_vasd'}
 })
  .then((res) => res.json())
  .then((data) => {
      console.log(data.docs[0]);
      quote.innerHTML = `<cite class="fs-2 color-dark typo-txt txt-center">"${data.docs[0].dialog}"</cite><span class="color-dark typo-txt txt-right" id="author"></span><span class="color-dark typo-txt txt-right">API the one (lotr)</span>`;
      const author = document.getElementById('author')
      let character = data.docs[0].character;
      console.log(typeof character);
      let urlCharacter = "https://the-one-api.dev/v2/character/" + character;
      console.log(urlCharacter);
      fetch(urlCharacter, {method:'GET',
      headers: {'Authorization': 'Bearer '+ 'rQbJd3RkXyanIew_vasd'}
     })
      .then((res) => res.json())
      .then((data) => {
          console.log(data.docs[0].name);
          characterName = data.docs[0].name;
          author.innerHTML = characterName;
      });
  });  
}
getQuote();
quote.addEventListener('click', () => {
  getQuote();
});
// SMOOTH CONTROL
$(document).ready(function(){
    // Add smooth scrolling to all links
    $("a").on('click', function(event) {
      // Make sure this.hash has a value before overriding default behavior
      if (this.hash !== "") {
        // Prevent default anchor click behavior
        event.preventDefault();
          var hash = this.hash;
        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 1000, function(){
          window.location.hash = hash;
        });
      } 
    });
});

    
window.addEventListener('scroll', () => {
    const scrollY = this.scrollY;
    if (scrollY > 670) {
        back.style.top ="90vh";
      }
    else {
        back.style.top ="-50vh";
    }
})